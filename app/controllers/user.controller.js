const db = require("../models");
const User = db.users;
const Company = db.companies;
const WorkingDays = db.workingDays;
const UsersWorkingDays = db.usersWorkingDays;
const Op = db.Sequelize.Op;

// Create and Save a new User
exports.create = (req, res) => {
  // Validate request
  if (!req.body.firstName) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a User
  const user = {
    firstName: req.body.firstName,
    email: req.body.email,
    lastName: req.body.lastName,
    companyId: req.body.companyId,
  };

  // Save User in the database
  for (let index = 0; index < 1000; index++) {
    User.create(user)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the User."
        });
      });
  }
};

// Retrieve all Users from the database.
exports.findAll = (req, res) => {
  User.findAll({
      include: { model: Company, as: 'company' }
    })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving users."
      });
    });
};

// Find a single User with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findOne({
    where: {email: "daniuska@gmail.com"}, include: 'company', model: Company
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id
      });
    });
};