const db = require("../models");
const WorkingDays = db.workingDays;
const Op = db.Sequelize.Op;

// Create and Save a new WorkingDays
exports.create = (req, res) => {
  // Validate request
  if (!req.body.weekDay) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a WorkingDays
  const workingDays = {
    weekDay: req.body.weekDay,
    workingDate: req.body.workingDate,
    isWorking: req.body.isWorking,
  };

  // Save WorkingDays in the database
  for (let index = 0; index < 1000; index++) {
    WorkingDays.create(workingDays)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the WorkingDays."
        });
      });
  }
};

// Retrieve all WorkingDays from the database.
exports.findAll = (req, res) => {
  WorkingDays.findAll({ 
    // include: { as: 'usersWorkingDayss', model: User }
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving WorkingDays."
      });
    });
};

// Find a single WorkingDays with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  WorkingDays.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving WorkingDays with id=" + id
      });
    });
};