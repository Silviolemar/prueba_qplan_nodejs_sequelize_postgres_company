const db = require("../models");
const UsersWorkingDays = db.usersWorkingDays;
const Op = db.Sequelize.Op;

// Create and Save a new usersWorkingDays
exports.create = (req, res) => {
  // Validate request
  if (!req.body.userId) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a usersWorkingDays
  const usersWorkingDays = {
    userId: req.body.userId,
    workingDayId: req.body.workingDayId
  };

  // Save usersWorkingDays in the database
  for (let index = 0; index < 1000; index++) {
    UsersWorkingDays.create(usersWorkingDays)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the usersWorkingDays."
        });
      });
  }
};

// Retrieve all usersWorkingDays from the database.
exports.findAll = (req, res) => {
  UsersWorkingDays.findAll({ 
    // include: { as: 'usersusersWorkingDayss', model: User }
  })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving usersWorkingDays."
      });
    });
};

// Find a single usersWorkingDays with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  UsersWorkingDays.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving usersWorkingDays with id=" + id
      });
    });
};