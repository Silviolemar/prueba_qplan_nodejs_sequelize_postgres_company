module.exports = app => {
    const order = require("../controllers/workingDay.controller.js");
  
    var router = require("express").Router();
  
    // Create a new order
    router.post("/", order.create);
  
    // Retrieve all order
    router.get("/", order.findAll);
  
    // Retrieve a single order with id
    router.get("/:id", order.findOne);
  
    app.use("/api/workingDays", router);
  };
  