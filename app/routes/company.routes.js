module.exports = app => {
    const service = require("../controllers/company.controller.js");
  
    var router = require("express").Router();
  
    // Create a new service
    router.post("/", service.create);
  
    // Retrieve all service
    router.get("/", service.findAll);
  
    // Retrieve a single service with id
    router.get("/:id", service.findOne);
  
    app.use("/api/companies", router);
  };
  